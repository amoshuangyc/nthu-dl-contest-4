import torch
from torch import nn
from torch.nn import functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from vocab import vocab


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)


class CaptionLSTM(nn.Module):
    def __init__(self):
        super().__init__()
        self.ebd = nn.Embedding(len(vocab), 256)
        self.lstm = nn.LSTM(256, 512, batch_first=True)
        self.fc = nn.Linear(512, 256)

    def forward(self, cap_b, len_b):
        N = cap_b.size(0)
        cap_b = self.ebd(cap_b) # [N, S, 256]
        lstm_inp_b = pack_padded_sequence(cap_b, len_b, batch_first=True)
        lstm_out_b, _ = self.lstm(lstm_inp_b)
        cond_b, len_b = pad_packed_sequence(lstm_out_b, batch_first=True)
        cond_b = cond_b[torch.arange(N).long(), len_b - 1, :] # [N, 512]
        cond_b = self.fc(cond_b) # [N, 256]
        return cond_b


class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.ebd = CaptionLSTM()
        self.main = nn.Sequential(
            nn.ConvTranspose2d(356, 1024, 4, 1, 0, bias=False),
            nn.BatchNorm2d(1024),
            nn.ReLU(True),
            nn.ConvTranspose2d(1024, 512, 4, 2, 1, bias=False),
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            nn.ConvTranspose2d(512, 256, 4, 2, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            nn.ConvTranspose2d(256, 128, 4, 2, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),
            nn.ConvTranspose2d(128, 64, 4, 2, 1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(True),
            nn.ConvTranspose2d(64, 3, 4, 2, 1, bias=False),
            nn.Sigmoid()
        )

    def forward(self, cap_b, len_b):
        N = cap_b.size(0)
        device = cap_b.device
        cond_b = self.ebd(cap_b, len_b)
        cond_b = cond_b.view(N, -1, 1, 1)
        noise_b = torch.randn(N, 100, 1, 1, device=device)
        x = torch.cat((cond_b, noise_b), dim=1)
        return self.main(x)


class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        self.ebd = CaptionLSTM()
        self.convs = nn.Sequential(
            nn.Conv2d(3, 64, 4, 4, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(64, 128, 4, 2, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(128, 256, 4, 2, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(256, 512, 4, 2, 1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(512, 512, 4, 1, 0, bias=False),
            nn.Sigmoid()
        )
        self.classifier = nn.Sequential(
            nn.Linear(512 + 256, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(512, 256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(256, 1),
            nn.Sigmoid()
        )

    def forward(self, img_b, cap_b, len_b):
        N = img_b.size(0)
        cond_b = self.ebd(cap_b, len_b)
        img_b = self.convs(img_b)
        img_b = img_b.view(N, -1)
        inp_b = torch.cat((img_b, cond_b), dim=1)
        out_b = self.classifier(inp_b)
        return out_b
