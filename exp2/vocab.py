import numpy as np

class RawVocab:
    def __init__(self):
        self.word2idx = dict()
        self.idx2word = dict()
        for idx, word in np.load('../raw/dictionary/id2Word.npy'):
            idx = int(idx)
            self.word2idx[word] = idx
            self.idx2word[idx] = word
    
    def indices(self, words):
        return [self.word2idx[word] for word in words]

    def wordify(self, indices):
        return [self.idx2word[idx] for idx in indices]


class Vocab:
    def __init__(self):
        self.word2idx = dict()
        self.idx2word = dict()

        for word in ['<pad>', '<ukn>', '<sos>', '<eos>']:
            self.add_word(word)

        for word, cnt in np.load('../raw/dictionary/vocab.npy'):
            if int(cnt) >= 3:
                self.add_word(word)

    def __len__(self):
        return len(self.word2idx)

    def add_word(self, word):
        idx = len(self.word2idx)
        word = word.lower()
        self.word2idx[word] = idx
        self.idx2word[idx] = word

    def indices(self, words):
        ukn_idx = self.word2idx['<ukn>']
        return [self.word2idx.get(word, ukn_idx) for word in words]

    def wordify(self, indices):
        indices = indices.numpy().tolist()
        ukn_idx = self.word2idx['<ukn>']
        words = [self.idx2word.get(idx, ukn_idx) for idx in indices]
        return words

raw_vocab = RawVocab()
vocab = Vocab()