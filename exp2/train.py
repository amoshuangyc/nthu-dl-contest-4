import json
import random
import numpy as np
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from datetime import datetime
import toyplot
import toyplot.html

import torch
from torch.nn import functional as F
from torch.utils.data import Subset, DataLoader
from torchvision.utils import save_image

seed = 999
random.seed(seed)
torch.manual_seed(seed)

from data import Dataset
from model import Generator
from model import Discriminator
from model import weights_init


device = 'cuda'
G = Generator().to(device)
D = Discriminator().to(device)
G.apply(weights_init)
D.apply(weights_init)
optimG = torch.optim.Adam(G.parameters(), lr=1e-3, betas=(0.5, 0.999))
optimD = torch.optim.Adam(D.parameters(), lr=1e-3, betas=(0.5, 0.999))

train_set = Dataset()
visul_set = Subset(train_set, random.sample(range(len(train_set)), 128))
train_loader = DataLoader(train_set, 64, shuffle=True, collate_fn=Dataset.collate_fn)
visul_loader = DataLoader(visul_set, 64, shuffle=False, collate_fn=Dataset.collate_fn)

log_dir = Path('./log/') / '{:%Y.%m.%d-%H:%M:%S}'.format(datetime.now())
log_dir.mkdir(parents=True)


def train(pbar):
    G.train()
    D.train()
    metrics = {
        'lossG': 0.0,
        'lossD': 0.0
    }
    for img_real_b, img_rand_b, cap_b, len_b in iter(train_loader):
        N = img_real_b.size(0)
        img_real_b = img_real_b.to(device)
        img_rand_b = img_rand_b.to(device)
        cap_b = cap_b.to(device)
        len_b = len_b.to(device)

        optimD.zero_grad()
        img_fake_b = G(cap_b, len_b)
        y_real_pred = D(img_real_b, cap_b, len_b)
        y_real_true = torch.full((N, 1), 1.0, device=device)
        loss_real = F.binary_cross_entropy(y_real_pred, y_real_true)
        loss_real.backward()
        y_rand_pred = D(img_rand_b, cap_b, len_b)
        y_rand_true = torch.full((N, 1), 0.0, device=device)
        loss_rand = F.binary_cross_entropy(y_rand_pred, y_rand_true)
        loss_rand.backward()
        y_fake_pred = D(img_fake_b, cap_b, len_b)
        y_fake_true = torch.full((N, 1), 0.0, device=device)
        loss_fake = F.binary_cross_entropy(y_fake_pred, y_fake_true)
        loss_fake.backward()
        lossD = loss_real.detach() + loss_rand.detach() + loss_fake.detach()
        optimD.step()

        optimG.zero_grad()
        img_fake_b = G(cap_b, len_b)
        y_fake_pred = D(img_fake_b, cap_b, len_b)
        y_fake_true = torch.full((N, 1), 1.0, device=device)
        lossG = F.binary_cross_entropy(y_fake_pred, y_fake_true)
        lossG.backward()
        optimG.step()

        metrics['lossD'] = lossD.item()
        metrics['lossG'] = lossG.item()
        pbar.set_postfix(metrics)
        pbar.update(N)
    return metrics


def visul(epoch, pbar):
    G.eval()
    D.eval()
    epoch_dir = log_dir / f'{epoch:03d}'
    epoch_dir.mkdir()
    for img_real_b, _, cap_b, len_b in iter(visul_loader):
        N = img_real_b.size(0)
        cap_b = cap_b.to(device)
        len_b = len_b.to(device)
        img_fake_b = G(cap_b, len_b).cpu()
        vis_imgs = torch.cat((img_real_b, img_fake_b), dim=0)
        vis_path = epoch_dir / f'{pbar.n:05d}.jpg'
        save_image(vis_imgs, vis_path, nrow=16)
        pbar.update(N)

def log(epoch, train_metrics):
    json_path = log_dir / 'loss.json'
    if json_path.exists():
        df = pd.read_json(json_path)
    else:
        df = pd.DataFrame()

    metrics = {'epoch': epoch, **train_metrics}
    df = df.append(metrics, ignore_index=True)
    df = df.astype('str').astype('float')
    with json_path.open('w') as f:
        json.dump(df.to_dict(orient='records'), f, indent=2)

    canvas = toyplot.Canvas(700, 300)
    style = {"text-anchor":"start", "-toyplot-anchor-shift":"5px"}
    axes = canvas.cartesian()
    for key in ['lossG', 'lossD']:
        axes.plot(np.arange(len(df)), df[key])
        axes.text(len(df) - 1, df[key].iloc[-1], key, style=style)
    toyplot.html.render(canvas, str(log_dir / 'log.html'))

    if epoch % 50 == 0:
        torch.save(G.state_dict(), log_dir / f'{epoch:03d}/G.pth')
        torch.save(D.state_dict(), log_dir / f'{epoch:03d}/D.pth')


for epoch in range(800):
    print(f'Epoch {epoch:03d}')
    with tqdm(total=len(train_set), desc='  Train', ascii=True) as pbar:
        train_metrics = train(pbar)
    with torch.no_grad():
        with tqdm(total=len(visul_set), desc='  Visul', ascii=True) as pbar:
            visul(epoch, pbar)
        log(epoch, train_metrics)
