import json
import pickle
import random
from tqdm import tqdm
from PIL import Image
from pathlib import Path

import torch
from torchvision import datasets
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import pad_sequence
from torchvision.transforms import functional as tf

from vocab import raw_vocab
from vocab import vocab


class Prepare:
    def __init__(self, img_size):
        self.src_dir = Path('../raw/dataset/')
        self.dst_dir = Path('./data/')
        self.dst_dir.mkdir(exist_ok=True, parents=True)

        with open('../raw/dataset/text2ImgData.pkl', 'rb') as f:
            data = pickle.load(f).to_dict(orient='records')

        self.anns = []
        for item in tqdm(data):
            img_path = self.src_dir / item['ImagePath']
            img_name = img_path.name
            img = Image.open(img_path).convert('RGB')
            img = img.resize(img_size)
            img.save(self.dst_dir / img_name)

            captions = [
                raw_vocab.wordify(map(int, cap))
                for cap in item['Captions']
            ]
            self.anns.append({
                'img_name': img_name,
                'captions': captions,
            })

        with (self.dst_dir / 'anns.json').open('w') as f:
            json.dump(self.anns, f, indent=2, ensure_ascii=False)


class Dataset:
    def __init__(self):
        self.img_dir = Path('./data/')
        with open('./data/anns.json') as f:
            self.anns = json.load(f)

    def __len__(self):
        return len(self.anns)

    def __getitem__(self, idx):
        ann_real = self.anns[idx]
        img_real = Image.open(self.img_dir / ann_real['img_name'])
        img_real = img_real.convert('RGB')
        img_real = tf.to_tensor(img_real)

        ann_rand = random.choice(self.anns)
        img_rand = Image.open(self.img_dir / ann_rand['img_name'])
        img_rand = img_rand.convert('RGB')
        img_rand = tf.to_tensor(img_rand)

        cap = random.choice(ann_real['captions'])
        cap = [word for word in cap if word != '<PAD>']
        cap = ['<sos>'] + cap + ['<eos>']
        cap = vocab.indices(cap)
        cap = torch.tensor(cap).long()
        return img_real, img_rand, cap

    @staticmethod
    def collate_fn(batch):
        '''
        Return:
            img_real_b: (FloatTensor) sized [N, H, W]
            img_rand_b: (FloatTensor) sized [N, H, W]
            cap_b: (LongTensor) sized [N, S]
            len_b: (LongTensor) sized [N]
        '''
        # sort batch data by caption length
        batch.sort(key=lambda x: len(x[-1]), reverse=True)

        img_real_b, img_rand_b, cap_b = zip(*batch)
        img_real_b = torch.stack(img_real_b, dim=0)
        img_rand_b = torch.stack(img_rand_b, dim=0)
        len_b = torch.tensor(list(map(len, cap_b))).long()
        cap_b = pad_sequence(cap_b, batch_first=True)

        return img_real_b, img_rand_b, cap_b, len_b

if __name__ == '__main__':
    Prepare((64, 64))

    ds = Dataset()
    img_real, img_fake, cap = ds[-1]
    print(vocab.wordify(cap))

    dl = DataLoader(ds, batch_size=16, collate_fn=ds.collate_fn)
    img_real_b, img_rand_b, cap_b, len_b = next(iter(dl))
    print(img_real_b.size())
    print(img_rand_b.size())
    print(cap_b.size())
    print(len_b.size())
