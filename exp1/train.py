import random
from tqdm import tqdm
from pathlib import Path
from datetime import datetime

import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torchvision.utils import save_image

from data import Dataset
from model import Generator, Discriminator, weights_init


seed = 999
random.seed(seed)
torch.manual_seed(seed)

device = 'cuda'
G = Generator().to(device)
D = Discriminator().to(device)
G.apply(weights_init)
D.apply(weights_init)
optimG = torch.optim.Adam(G.parameters(), lr=2e-3, betas=(0.5, 0.999))
optimD = torch.optim.Adam(D.parameters(), lr=2e-3, betas=(0.5, 0.999))

train_set = Dataset()
train_loader = DataLoader(train_set, 256, shuffle=True, collate_fn=Dataset.collate_fn)
visul_noise_b = torch.randn(256, 100, 1, 1, device=device)

log_dir = Path('./log/') / '{:%Y.%m.%d-%H:%M:%S}'.format(datetime.now())
log_dir.mkdir(parents=True)


def train(pbar):
    G.train()
    D.train()
    metrics = {
        'lossG': 0.0,
        'lossD': 0.0
    }
    for img_real_b, _, _, _ in iter(train_loader):
        N = img_real_b.size(0)
        img_real_b = img_real_b.to(device)

        optimD.zero_grad()
        noise_b = torch.randn(N, 100, 1, 1, device=device)
        img_fake_b = G(noise_b)
        y_real_pred = D(img_real_b)
        y_real_true = torch.full((N, 1), 1.0, device=device)
        loss_real = F.binary_cross_entropy(y_real_pred, y_real_true)
        loss_real.backward()
        y_fake_pred = D(img_fake_b)
        y_fake_true = torch.full((N, 1), 0.0, device=device)
        loss_fake = F.binary_cross_entropy(y_fake_pred, y_fake_true)
        loss_fake.backward()
        lossD = loss_real.detach() + loss_fake.detach()
        optimD.step()

        optimG.zero_grad()
        noise_b = torch.randn(N, 100, 1, 1, device=device)
        img_fake_b = G(noise_b)
        y_fake_pred = D(img_fake_b)
        y_fake_true = torch.full((N, 1), 1.0, device=device)
        lossG = F.binary_cross_entropy(y_fake_pred, y_fake_true)
        lossG.backward()
        optimG.step()

        metrics['lossD'] = lossD.item()
        metrics['lossG'] = lossG.item()
        pbar.set_postfix(metrics)
        pbar.update(N)
    return metrics


def visul(epoch):
    G.eval()
    D.eval()
    img_fake_b = G(visul_noise_b).cpu()
    vis_path = log_dir / f'{epoch:03d}.jpg'
    save_image(img_fake_b, vis_path, nrow=32)


for epoch in range(50):
    print(f'Epoch {epoch:03d}')
    with tqdm(total=len(train_set), desc='  Train', ascii=True) as pbar:
        train_metrics = train(pbar)
    with torch.no_grad():
        visul(epoch)
